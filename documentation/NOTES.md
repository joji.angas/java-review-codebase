## Objectives:

### Database (SQL)
1. Create DDL
    - Create database - **DONE**
    - Create schema - **DONE**
    - Create tables - **DONE**
    - Create indexes - **DONE**
    - Create custom indexes - **DONE**
2. Create DML
    - create insert statements for tables - **DONE**
    - create update statements for tables
    - create delete statements for tables
3. Create DQL
    - create specialised select statements for tables
    - create custom views
4. Create Stored Procedures

### OOP
1. Create samples for the 4 pillars of OOP
   - Inheritance
   - Polymorphism
   - Encapsulation
   - Abstraction
2. saadsda

### Java
- Create examples for Exception handling
  - Try / Catch / Finally block
  - Try with resources
  - ControllerAdvice (Spring/Spring MVC/REST)
- Create examples for Multithreading
  - Create thread using Thread child class
  - Create thread using class that implements Runnable
  - Create threads using Executor class
  - Create threads using ExecutorService