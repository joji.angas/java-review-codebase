# Developer Notes

## OOP

- **OOP - Object-Oriented Programming** - It is a programming paradigm where every 
  component is represented as **Objects**.<br><br>
  
  The template for objects is called a **Class**. An **Object** is an instance of a 
  **Class**.<br><br>

  Each object contains attributes - the object's instance variables, as well as 
  behaviour - the object's methods. These properties of objects define how these objects can be related, how they communicate, and what data they pass amongst themselves.
  
  ### 4 Pillars of OOP

  - **Inheritance** - Classes can inherit attributes and behaviour from other classes, 
    thus exhibiting a 'Is a' behaviour. These properties, though same for both parent 
    and child class, may exhibit different details on their implementation. When a child 
    class provides different implementation for a parent behaviour or attribute, this is called **overriding**.
  - **Encapsulation** - Classes can hide, or 'limit' access to their attributes and behaviour.
  - **Polymorphism** - Classes can expose behaviours of the same name with varying parameters. 
    This is through **method overloading**. 
  - **Abstraction** - Classes can expose their data and functionality without the need to 
    expose the details of the implementation.

## Database

- **DDL - Data Definition Language** (CREATE, DROP, ALTER, TRUNCATE)
- **DML - Data Manipulation Language** (INSERT, UPDATE, DELETE)
- **DQL - Data Query Language** (SELECT)
- **ERD - Entity Relationship Diagram** - A diagram showing the various entities that would be defined on your database. It also shows the relationship between these entities.