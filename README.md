## JAVA REVIEW CODEBASE

This was inspired by a review session that I got 
from a company that I am working for.

This contains code samples that would exhibit several 
features and characteristics of the Java programming language. 
It would also exhibit some features from other Java-based APIs 
and frameworks such as Hibernate, Spring / Spring Boot, Lombok, 
Springdoc OpenAPI, and others.

### Other reference materials

For design documentation, and other related documents
generated for this project, please see the 
Documentation directory on the project root.