INSERT INTO JAVA_REVIEW.PRODUCT_CATEGORIES (CATEGORY_NAME) VALUES
	 ('Undergarments'),
	 ('Toiletries'),
	 ('Accessories'),
	 ('School Supplies'),
	 ('Adult Items');

INSERT INTO JAVA_REVIEW.REGIONS (REGION_NAME) VALUES
	 ('South America'),
	 ('North America'),
	 ('Asia Pacific'),
	 ('European Union'),
	 ('Australia and New Zealand');

INSERT INTO JAVA_REVIEW.COUNTRIES (COUNTRY_NAME,REGION_ID) VALUES
	 ('United States of America',2),
	 ('Argentina',1),
	 ('Hawaii',3),
	 ('Germany',4),
	 ('Australia',5);

INSERT INTO JAVA_REVIEW.LOCATIONS (COUNTRY_ID,ADDRESS,POSTAL_CODE,CITY,STATE) VALUES
	 (1,'123 San Jose','90845','Los Angeles','California'),
	 (4,'36th Floor Gutenberg Building','34567','Berlin',NULL),
	 (5,'235 Sydney Harbour','6778899','Sydney',NULL);

INSERT INTO JAVA_REVIEW.WAREHOUSES (WAREHOUSE_NAME,LOCATION_ID) VALUES
	 ('US-LA-1',1),
	 ('DE-BER-1',2),
	 ('AU-SYD-1',3);

INSERT INTO JAVA_REVIEW.PRODUCTS (PRODUCT_NAME,DESCRIPTION,STANDARD_COST,LIST_PRICE,CATEGORY_ID) VALUES
	 ('Victoria''s Secret Night Thong','See-through women''s underwear, black',230.0,249.0,5),
	 ('Bench Body V-Neck Sleeveless','V-neck sleeveless shirt, white',45.0,50.0,1),
	 ('Aksesoriz Neon Bangles','Neon coloured bangles',15.0,20.0,3),
	 ('Staedtler HB-200','Pencil, HB, yellow',3.0,5.0,4);


INSERT INTO JAVA_REVIEW.INVENTORIES (PRODUCT_ID,WAREHOUSE_ID,QUANTITY) VALUES
	 (4,2,200),
	 (2,3,300),
	 (3,3,100);

INSERT INTO JAVA_REVIEW.EMPLOYEES (EMPLOYEE_ID, FIRST_NAME,LAST_NAME,EMAIL,PHONE,HIRE_DATE,MANAGER_ID,JOB_TITLE) VALUES
	 (1,'John','Norris','john.norris@yourshop.com','43546565','2016-02-14',NULL,'Head of Logistics'),
	 (2,'Michael','Sanders','mike.sanders@yourshop.com','22245678','2019-11-11',1,'Logistics Manager'),
	 (3,'Edouardo','Vasquez','edo.vasquez@yourshop.com','67677878','2018-04-18',2,'Logistics Staff'),
	 (4,'Ching','Yuwai','ching.yuwai@yourshop.com','45546578','2017-08-16',NULL,'Sales Manager'),
	 (5,'Alice','Chambers','alice.chambers@yourshop.com','67768788','2019-03-19',4,'Clerk - Sales');

INSERT INTO JAVA_REVIEW.CUSTOMERS (NAME,ADDRESS,WEBSITE,CREDIT_LIMIT) VALUES
	 ('New Life Retirement Home','235 4th Street, Brooklyn, New York','www.newlife-retirement.com','50000'),
	 ('John Smith','30th Floor New Era Building, San Diego, California','www.john-smith.com','20000');


INSERT INTO JAVA_REVIEW.CONTACTS (FIRST_NAME,LAST_NAME,EMAIL,PHONE,CUSTOMER_ID) VALUES
	 ('John','Smith','john.smith@john-smith.com','5446566',2),
	 ('Martha','Smith','martha.smith@john-smith.com','5656556',2),
	 ('Helena','Valatova','helena.v@newlife-retirement.com','7678788',1);

INSERT INTO JAVA_REVIEW.ORDERS (CUSTOMER_ID,STATUS,SALESMAN_ID,ORDER_DATE) VALUES
	 (2,'PAID',5,'2013-12-29'),
	 (1,'ORDERED',5,'2012-09-18'),
	 (2,'PACKAGED',5,'2022-11-08'),
	 (2,'SHIPPED',5,'2021-12-04'),
	 (1,'RECEIVED',5,'2020-05-13');


INSERT INTO JAVA_REVIEW.ORDER_ITEMS (ORDER_ID,PRODUCT_ID,QUANTITY,UNIT_PRICE) VALUES
	 (1,1,7,0.00),
	 (2,4,15,0.00),
	 (3,2,5,0.00);


