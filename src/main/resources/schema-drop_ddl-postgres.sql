DROP TABLE public.contacts CASCADE;

DROP TABLE public.countries CASCADE;

DROP TABLE public.customers CASCADE;

DROP TABLE public.employees CASCADE;

DROP TABLE public.inventories CASCADE;

DROP TABLE public.locations CASCADE;

DROP TABLE public.order_items CASCADE;

DROP TABLE public.orders CASCADE;

DROP TABLE public.product_categories CASCADE;

DROP TABLE public.products CASCADE;

DROP TABLE public.regions CASCADE;

DROP TABLE public.warehouses CASCADE;
