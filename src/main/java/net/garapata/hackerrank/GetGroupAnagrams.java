package net.garapata.hackerrank;

import java.util.ArrayList;
import java.util.List;

public class GetGroupAnagrams {
    List<String> anagrams;

    public boolean checkIfAnagrams(String str1, String str2, List<String> anaList){
        boolean isAnagram = false;
        generateAnagramList(str1, 0, str1.length() - 1);
        if(anagrams == null) throw new NullPointerException();

        return isAnagram;
    }

    private void generateAnagramList(String str, int start, int end) {
        anagrams = new ArrayList<String>();
        if (start == end) {
//            System.out.println(str);
            anagrams.add(str);
            System.out.println(str + " added");
        } else {
            for (int i = start; i <= end; i++) {
                str = swap(str, start, i);
                generateAnagramList(str, start + 1, end);
                str = swap(str, start, i);
            }
        }
    }

    public String swap(String a, int i, int j) {
        char temp;
        char[] charArray = a.toCharArray();
        temp = charArray[i];
        charArray[i] = charArray[j];
        charArray[j] = temp;
        return String.valueOf(charArray);
    }


    public static void main(String[] args) {
        String str1 = "notable";
        String str2 = "notbale";
        int size = str1.length();
        GetGroupAnagrams getGroupAnagrams = new GetGroupAnagrams();

        System.out.println("String is: " + str1);
//        System.out.println("Anagram of the given string is:-");
        System.out.println("Is " + str2 + " an anagram of " + str1 + " ? " + getGroupAnagrams.checkIfAnagrams(str1, str2, null));
    }
}
