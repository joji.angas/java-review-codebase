package net.garapata.hackerrank;

public class FizzBuzzTest {


    public void fizzBuzz(int n){
        for (int x=0; x<=n; x++){
            if (isMod(3, x) && isMod(5, x)) {
                System.out.println("FizzBuzz");
            } else if (isMod(3, x)) {
                System.out.println("Fizz");
            } else if(isMod(5, x)) {
                System.out.println("Buzz");
            } else {
                System.out.println(x);
            }
        }
    }

    public static boolean isMod(int dividend, int number){
        return (dividend % number) == 0;
    }

    public static void main(String[] args) {

    }

}
