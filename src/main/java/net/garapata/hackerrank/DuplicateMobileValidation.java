package net.garapata.hackerrank;

import lombok.Getter;

public class DuplicateMobileValidation {

    private DuplicateMobileValidation(){}

    @Getter
    static class ContactDetail {
        String mobile;
        String alternateMobile;
        String landLine;
        String email;
        String address;

        public ContactDetail() {
        }

        public ContactDetail(String mobile, String alternateMobile, String landLine, String email, String address) {
            this.mobile = mobile;
            this.alternateMobile = alternateMobile;
            this.landLine = landLine;
            this.email = email;
            this.address = address;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public void setAlternateMobile(String alternateMobile) {
            this.alternateMobile = alternateMobile;
        }

        public void setLandLine(String landLine) {
            this.landLine = landLine;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        @Override
        public String toString() {
            return  "Mobile:" + mobile + "\n" +
                    "Alternate mobile:" + alternateMobile + "\n" +
                    "LandLine:" + landLine + "\n" +
                    "Email:" + email + "\n" +
                    "Address:" + address + "\n";
        }
    }

    public static class DuplicateMobileNumberException extends Exception {
        public static final String ERR_MSG = "Mobile number and alternate mobile number are the same";

        public DuplicateMobileNumberException(){
            super(ERR_MSG);
        }
    }

    static class ContactDetailBO {

        private ContactDetailBO(){ }

        static void validate(String mobile, String alternateMobile) throws DuplicateMobileNumberException {
            if(mobile.equals(alternateMobile)) { throw new DuplicateMobileNumberException(); }
        }
    }
}
