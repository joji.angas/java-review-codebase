package net.garapata.exceptions;

import net.garapata.oop.model.DemoParent;

import java.io.Closeable;
import java.io.IOException;

public class TryWithResourcesDemo extends DemoParent implements Closeable  {

    public TryWithResourcesDemo(String topic, String description, String definition){
        this.demoFor = "Try with resources statement";
        this.demoDefinition = "This is a Try statement where resources that need to be destroyed,\n " +
                "or closed are formally terminated automatically without the need for a finally block.";
        this.demoDescription = "This demo demonstrates how a custom class can be used in a Try with resources block.\n " +
                "The custom class needs to implement \'Closeable\' interface, and provide implementation to the close() method.";
    }

    public TryWithResourcesDemo(){
        
        System.out.println("Starting the class instance.");
    }

    public void doSomething(){
        System.out.println("Doing something...");
        System.out.println("Done.");
    }

    @Override
    public void close() throws IOException {
        System.out.println("Closing the class instance.");
    }

    @Override
    public String showDescription() {
        return null;
    }


    public static void main(String[] args) throws IOException {
        try(TryWithResourcesDemo twrDemo = new TryWithResourcesDemo();){
            // do something here
            twrDemo.doSomething();
        }
    }
}
