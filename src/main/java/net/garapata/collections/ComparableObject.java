package net.garapata.collections;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ComparableObject implements Comparable<ComparableObject> {
    private String firstName;
    private String lastName;
    private long id;

    @Override
    public int hashCode() {
        return firstName.hashCode() + lastName.hashCode() + Long.valueOf(id).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return (this.firstName.equals(((ComparableObject) obj).getFirstName()) && this.lastName.equals(((ComparableObject) obj).getLastName()) && Long.valueOf(this.id).equals(((ComparableObject) obj).getId()));
    }
    @JsonIgnore
    public String getFullName(){
        return lastName + ", "  + firstName;
    }

    @Override
    public int compareTo(ComparableObject o) {
        return getFullName().compareTo(o.getFullName());
    }
}
