package net.garapata.collections;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IterableObject {
    private String name;
    private String id;
    private int hash;

    @Override
    public int hashCode() {
        hash = name.hashCode() + id.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return (this.id.equals(((IterableObject) obj).getId()) && this.name.equals(((IterableObject) obj).getName()));
    }
}
