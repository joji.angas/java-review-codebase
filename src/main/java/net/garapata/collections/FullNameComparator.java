package net.garapata.collections;

import java.util.Comparator;

public class FullNameComparator implements Comparator<ComparableObject> {
    @Override
    public int compare(ComparableObject o1, ComparableObject o2) {
        return o1.compareTo(o2);
    }
}
