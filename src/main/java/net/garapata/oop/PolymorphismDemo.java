package net.garapata.oop;

import net.garapata.oop.model.DemoParent;

public class PolymorphismDemo extends DemoParent {
    private static final String DESCRIPTION = "This class is a demonstration for the Polymorphism feature of OOP classes.";

    @Override
    public String showDescription() {
        return DESCRIPTION;
    }
}
