package net.garapata.oop.model;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
//@Embeddable
public class PersonWithContactInfo {
    private String first_name;
    private String last_name;
    private String email;
    private String phone;
}
