package net.garapata.oop.model;

import static java.lang.System.*;

public abstract class DemoParent {
    protected String demoDefinition;
    protected String demoFor;
    protected String demoDescription;
    protected boolean noParam;
    protected boolean singleParam;
    protected boolean multiParam;

    public DemoParent() {
        this.noParam = true;
        out.println("super() is called");
    }

    public abstract String showDescription();
}
