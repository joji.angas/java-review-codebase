package net.garapata.oop;

import lombok.extern.slf4j.Slf4j;

import static java.lang.System.*;

@Slf4j
public class InheritanceDemo extends net.garapata.oop.model.DemoParent {
    private static final String DESCRIPTION = "This class is a demonstration for the Inheritance feature of OOP classes.";

    public InheritanceDemo(){
        super();
        out.println("Explicit call to super() is done");
    }

    public InheritanceDemo(String topic){
        demoFor = topic;
        singleParam = true;
    }

    @Override
    public String showDescription() {
        return DESCRIPTION;
    }
}

class InheritanceDemo2 extends net.garapata.oop.model.DemoParent {

    public InheritanceDemo2(){
        out.println("No explicit call to super() is done");
    }

    public InheritanceDemo2(String topic, String definition, String description){
        demoFor = topic;
        demoDefinition = definition;
        demoDescription = description;
        multiParam = true;
    }

    @Override
    public String showDescription() {
        return demoDescription;
    }
}

/*
    class ParentSample {
     */
        /*
            Implementation is commented out because regardless
            of having a method with the same signature
            with another parent class, the subclass would return
            compile-time errors for trying to inherit from multiple
            parent classes.

            public String showDescription(){
                return "Hello";
            }
        /*
    }
*/


/*
    class ChildSample extends DemoParent, ParentSample {
        /*
            This declaration shows an error on IDEs because
            a subclass cannot extend (inherit from) multiple
            classes because it would cause ambiguity in choosing
            which method or constructor to implement when multiple
            parent classes contain methods with the same signature,
            or the same constructor signatures
            (Diamond problem - https://www.geeksforgeeks.org/diamond-problem-in-java/).

            This can be remediated by implementing interfaces instead.
        /*
    }
*/

