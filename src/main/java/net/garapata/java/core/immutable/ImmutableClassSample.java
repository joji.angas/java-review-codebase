package net.garapata.java.core.immutable;

import lombok.Getter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

final class ImmutableClassSample {
    @Getter
    private final String description;
    private final HashMap<String, String> immutableMap;

    ImmutableClassSample(String description, HashMap<String, String> immutableMap) {
        this.description = description;

        HashMap<String, String> tempMap = new HashMap<>();
        Iterator<String> it = immutableMap.keySet().iterator();
        String key;
        while(it.hasNext()) {
            key = it.next();
            tempMap.put(key, immutableMap.get(key));
        }

        this.immutableMap = tempMap;
    }

    public Map<String, String> getImmutableMap(){
        return (HashMap<String, String>) immutableMap.clone();
    }

    @Override
    public int hashCode() {
        return description.hashCode() + immutableMap.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return (this.description.equals(((ImmutableClassSample) obj).getDescription()) && this.immutableMap.entrySet().equals(((ImmutableClassSample) obj).getImmutableMap().entrySet()));
    }
}
