package net.garapata.mvc.config.beans;

import net.garapata.db.model.Contacts;
import net.garapata.db.model.Customers;
import org.springframework.stereotype.Component;

@Component
public class SampleDataLoader {
    public void loadSampleCustomers(){
        Customers customer1 = Customers.builder()
                .name("John Smith")
                .address("143 Love St. Testing, Test, 123456")
                .website("www.johnsmith.com")
                .credit_limit(10000.00)
                .contactsList(null)
                .build();

        Customers customer2 = Customers.builder()
                .name("New Life Retirement Home")
                .address("143 Love St. Testing, Test, 123456")
                .website("www.newlife.com")
                .credit_limit(10000.00)
                .build();
    }

    public void loadSampleContacts(){
        Contacts contact1 = Contacts.builder()
                .first_name("John")
                .last_name("Smith")
                .email("john.smith@johnsmith.com")
                .phone("111 234 5678")
                .build();

        Contacts contact2 = Contacts.builder()
                .first_name("Marsha")
                .last_name("Smith")
                .email("marsha.smith@johnsmith.com")
                .phone("111 345 7890")
                .build();

        Contacts contact3 = Contacts.builder()
                .first_name("Mark")
                .last_name("Wahlburgh")
                .email("mark.wahl@newlife.com")
                .phone("111 456 5678")
                .build();

        Contacts contact4 = Contacts.builder()
                .first_name("Mike")
                .last_name("Shinodagibsen")
                .email("mike.shino@newlife.com")
                .phone("111 456 5678")
                .build();
    }
}
