package net.garapata.mvc.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/default")
public class DefaultController {

    @GetMapping(value = "/hi", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.ALL_VALUE }, produces = MediaType.APPLICATION_JSON_VALUE)
    public String home(){
        return "Hi!";
    }
}
