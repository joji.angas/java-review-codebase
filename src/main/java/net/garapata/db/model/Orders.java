package net.garapata.db.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Orders {
    @Id
    private long order_id;
    private long customer_id;
    private String status;
    private long salesman_id;
    private LocalDateTime order_date;
}
