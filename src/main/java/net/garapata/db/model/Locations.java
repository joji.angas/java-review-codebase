package net.garapata.db.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Locations {
    @Id
    private long location_id;
    private long country_id;
    private String address;
    private String postal_code;
    private String city;
    private String state;
}
