package net.garapata.db.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Inventories {
    @Id
    private long inventory_id;
    private long product_id;
    private long warehouse_id;
    private int quantity;
}
