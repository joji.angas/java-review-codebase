package net.garapata.db.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Collection;
import java.util.List;

@Entity
@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Customers {
    @Id
    private long customer_id;
    private String name;
    private String address;
    private String website;
    private double credit_limit;
    @Transient
    private List<Contacts> contactsList;

    public boolean add(Contacts contacts) {
        return contactsList.add(contacts);
    }

    public boolean isEmpty() {
        return contactsList.isEmpty();
    }

    public boolean contains(Object o) {
        return contactsList.contains(o);
    }

    public boolean remove(Object o) {
        return contactsList.remove(o);
    }

    public boolean addAll(Collection<? extends Contacts> c) {
        return contactsList.addAll(c);
    }
}
