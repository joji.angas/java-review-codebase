package net.garapata.db.model;

import lombok.*;
import net.garapata.oop.model.PersonWithContactInfo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Setter
@Getter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Contacts extends PersonWithContactInfo {
    @Id
    private long contact_id;
    private long customer_id;
    @Builder
    public Contacts(long contact_id, long customer_id, String first_name, String last_name, String email, String phone) {
        super(first_name, last_name, email, phone);
        this.contact_id = contact_id;
        this.customer_id = customer_id;
    }
}