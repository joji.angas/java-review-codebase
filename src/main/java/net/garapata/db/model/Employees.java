package net.garapata.db.model;

import lombok.*;
import net.garapata.oop.model.PersonWithContactInfo;

import javax.persistence.*;
import java.time.LocalDateTime;
@Entity
@Setter
@Getter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Employees extends PersonWithContactInfo {
    @Id
    private long employee_id;
    private LocalDateTime hire_date;
    private long manager_id;
    private String job_title;

    public Employees(long employee_id, String first_name, String last_name, String email, String phone, LocalDateTime hire_date, long manager_id, String job_title) {
        super(first_name, last_name, email, phone);
        this.hire_date = hire_date;
        this.manager_id = manager_id;
        this.job_title = job_title;
        this.employee_id = employee_id;
    }
}
