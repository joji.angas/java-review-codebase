package net.garapata.db.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderItems {
    @Id
    private long item_id;
    private long order_id;
    private long product_id;
    private int quantity;
    private int unit_price;
}
