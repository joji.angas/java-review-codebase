package net.garapata.db.model;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Countries {
    @Id
    private long country_id;
    private String country_name;
    private long region_id;
}
