package net.garapata.db.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Products {
    @Id
    private long product_id;
    private long category_id;
    private String product_name;
    private String description;
    private int standard_cost;
    private int list_price;
}
