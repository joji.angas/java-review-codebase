package net.garapata.db.repository;

import net.garapata.db.model.Customers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomersRepository extends JpaRepository<Customers, Long> {
    // all basic CRUD functions already available by default
}
