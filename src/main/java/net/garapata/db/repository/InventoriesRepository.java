package net.garapata.db.repository;

import net.garapata.db.model.Inventories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoriesRepository extends JpaRepository<Inventories, Long> {
}
