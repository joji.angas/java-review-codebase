package net.garapata.hackerrank;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import net.garapata.common.StaticTestDependencies;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.garapata.hackerrank.DuplicateMobileValidation.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@Slf4j
class DuplicateMobileValidationTest {

    ObjectWriter objectMapperWriter = StaticTestDependencies.getInstance().getObjectMapperWriter();
    Logger logger = LoggerFactory.getLogger(DuplicateMobileValidationTest.class);

    @DisplayName("Given Contact details with same primary and alternate mobile numbers, when validated for duplicate numbers, would throw an exception")
    @Test
    void testWithDuplicateMobileNumbers() throws JsonProcessingException {
        // Given
        ContactDetail contactDetail1  = new ContactDetail(
                "9874562310", "9874562310", "0447896541", "johndoe@abc.in", "22nd lane RR nagar kovai"
        );
        logger.info("\nContact Detail - With Duplicate Number: \n{}", objectMapperWriter.writeValueAsString(contactDetail1));

        // When
        Executable executable = () -> {
            ContactDetailBO.validate(contactDetail1.getMobile(), contactDetail1.getAlternateMobile());
        };

        // Then
        assertThrows(DuplicateMobileNumberException.class, executable);
    }

    @DisplayName("Given Contact details with different primary and alternate mobile numbers, when validated for duplicate numbers, would not throw an exception")
    @Test
    void testWithNoDuplicateNumbers() throws DuplicateMobileNumberException, JsonProcessingException {
        // Given
        ContactDetail contactDetail2 = new ContactDetail(
                "9874562310", "9874562311", "0447896541", "johndoe@abc.in", "22nd lane RR nagar kovai"
        );
        logger.info("\nContact Detail - With No Duplicate Number: \n{}", objectMapperWriter.writeValueAsString(contactDetail2));

        // When
        ContactDetailBO.validate(contactDetail2.getMobile(), contactDetail2.getAlternateMobile());

        // Then
        assertNotEquals(contactDetail2.getMobile(), contactDetail2.getAlternateMobile());
    }

}
