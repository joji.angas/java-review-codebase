package net.garapata.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class StaticTestDependencies {
    private ObjectMapper objectMapperDependency;

    private StaticTestDependencies(){
        init();
    }

    public static StaticTestDependencies getInstance(){
        return new StaticTestDependencies();
    }

    private void init(){
        this.objectMapperDependency = new ObjectMapper();
        this.objectMapperDependency.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    public ObjectWriter getObjectMapperWriter(){
        return objectMapperDependency.writerWithDefaultPrettyPrinter();
    }

}
