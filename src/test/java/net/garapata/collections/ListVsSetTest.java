package net.garapata.collections;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import net.garapata.common.StaticTestDependencies;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class ListVsSetTest {

    Logger logger = LoggerFactory.getLogger(ListVsSetTest.class);

    ObjectWriter objectMapperWriter = StaticTestDependencies.getInstance().getObjectMapperWriter();

    @DisplayName("Given a List and a Set with same elements, when checking insertion order, some element positions are not the same between two collections")
    @Test
    void testListVsSetInsertionOrder() throws JsonProcessingException {
        // Given
        ListVsSetSample lvsSample = new ListVsSetSample();
        lvsSample.setNamesList(Arrays.asList("John", "Matthew", "Oscar", "Sabrina"));
        Set<String> testSet = new HashSet<String>(Arrays.asList("John", "Matthew", "Oscar", "Sabrina"));
        lvsSample.setNamesSet(testSet);

        // When
        logger.info("\nList: \n{}",objectMapperWriter.writeValueAsString(lvsSample.getNamesList()));
        logger.info("\nSet: \n{}",objectMapperWriter.writeValueAsString(lvsSample.getNamesSet()));
        for(String hash: lvsSample.getNamesSet()){
            logger.info("\nhash: {} : {}", hash, hash.hashCode());
        }

        // Then
        assertNotEquals(new ArrayList<>(lvsSample.getNamesSet()).get(3), lvsSample.getNamesList().get(3));
    }

    @DisplayName("Given a group of Integers with two elements having the same value, when put on a Set, only one of the two elements with the same values is inserted on the Set")
    @Test
    void testHashSetContainingMultipleObjectsWithSameValue() throws JsonProcessingException {
        //Given
        Integer int1 = Integer.valueOf(11);
//        logger.info("int1: {}, hashCode: {}", int1, int1.hashCode());
        Integer int2 = Integer.valueOf(11);
//        logger.info("int2: {}, hashCode: {}", int2, int2.hashCode());
        Integer int3 = Integer.valueOf(12);
//        logger.info("int3: {}, hashCode: {}", int3, int3.hashCode());
        Integer int4 = Integer.valueOf(15);
//        logger.info("int4: {}, hashCode: {}", int4, int4.hashCode());

        // When
        Set<Integer> numberHashSet2 = new HashSet<Integer>(Arrays.asList(int1, int2, int3, int4));
        Iterator<Integer> itr2 = numberHashSet2.iterator();

        while(itr2.hasNext()){
            Integer curr2 = itr2.next();
            logger.info("Element: - value: {}, hashCode: {}", curr2, curr2.hashCode());
        }

        // Then
        logger.info("\nHashSet contents: \n{}", objectMapperWriter.writeValueAsString(numberHashSet2));
        assertTrue(numberHashSet2.contains(11));
    }


    @DisplayName("Given a group of custom objects with overridden equals() and hashCode() methods, with two elements having the same value, when put on a Set, only one of the two elements with the same values is inserted on the Set")
    @Test
    void testHashSetUsingCustomObjectAsElement() throws JsonProcessingException {
        // Given
        IterableObject it1 = IterableObject.builder().id("1").name("Joji").build();
        logger.info("it1 hash: {}", it1.hashCode());
        IterableObject it2 = IterableObject.builder().id("2").name("Joji").build();
        logger.info("it2 hash: {}", it2.hashCode());
        IterableObject it3 = IterableObject.builder().id("1").name("Joji").build();
        logger.info("it3 hash: {}", it3.hashCode());

        // When
        Set<IterableObject> iterableObjects = new HashSet<IterableObject>(Arrays.asList(it1, it2, it3));
        logger.info("\n{}", objectMapperWriter.writeValueAsString(iterableObjects));

        // Then
        assertEquals(it1, it3);
        logger.info("{} === {}", it1, it3);
        assertTrue(iterableObjects.contains(it1));
        assertTrue(iterableObjects.contains(it3));
        logger.info("iterableObjects Set contains {} and {}", it1, it3);
    }
}
