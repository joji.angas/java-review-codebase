package net.garapata.collections;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import net.garapata.common.StaticTestDependencies;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ComparableObjectTest {

    Logger logger = LoggerFactory.getLogger(ComparableObjectTest.class);

    ObjectWriter objectMapperWriter = StaticTestDependencies.getInstance().getObjectMapperWriter();

    @DisplayName("Given ComparableObjects implementing Comparable, and a FullNameComparator object implementing Comparator, when sorting the contents of a PriorityQueue, elements are sorted based on the criteria set by the Comparator")
    @Test
    void testComparableObjectsOrder() throws JsonProcessingException {
        // Given
        ComparableObject obj1 = ComparableObject.builder().firstName("Joji").lastName("Angas").id(1).build();
        ComparableObject obj2 = ComparableObject.builder().firstName("John Henry").lastName("Angas").id(2).build();
        ComparableObject obj3 = ComparableObject.builder().firstName("Romina Jane").lastName("Angas").id(3).build();
        ComparableObject obj4 = ComparableObject.builder().firstName("Alexandria Elise").lastName("Angas").id(4).build();


        // When
        PriorityQueue<ComparableObject> pq = new PriorityQueue<ComparableObject>(new FullNameComparator());
        pq.addAll(Arrays.asList(obj1, obj2, obj3, obj4));
        List<ComparableObject> co = new ArrayList<>(pq);
        Collections.sort(co, new FullNameComparator());

        // Then
        logger.info("\nUnordered: \n{}", objectMapperWriter.writeValueAsString(pq));
        logger.info("\nOrdered: \n{}", objectMapperWriter.writeValueAsString(co));
        assert pq.peek() != null;
        assertEquals(pq.peek().getFirstName(), "Alexandria Elise");
    }
}
