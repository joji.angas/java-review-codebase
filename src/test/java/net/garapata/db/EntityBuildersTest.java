package net.garapata.db;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import net.garapata.common.StaticTestDependencies;
import net.garapata.db.model.Contacts;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class EntityBuildersTest {

    Logger logger = LoggerFactory.getLogger(EntityBuildersTest.class);

    ObjectWriter objectMapperWriter = StaticTestDependencies.getInstance().getObjectMapperWriter();

    @DisplayName("Given Lombok-annotated Contact object, when object is instantiated using Lombok Builder, most of the details can be entered")
    @Test
    void testContactsConstructorBuilder() throws JsonProcessingException {
        // Given
        Contacts testContact = Contacts.builder()
            .first_name("John")
            .last_name("Smith")
            .phone("111-222-3456")
            .email("john.smith@example.com")
            .customer_id(023456)
            .contact_id(00001)
            .build();

        // When
        logger.info("\nContact: \n{}", objectMapperWriter.writeValueAsString(testContact));

        // Then
        assertNotNull(testContact);
    }
}
