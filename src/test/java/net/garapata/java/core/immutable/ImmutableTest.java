package net.garapata.java.core.immutable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import net.garapata.common.StaticTestDependencies;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Slf4j
class ImmutableTest {

    Logger logger = LoggerFactory.getLogger(ImmutableTest.class);

    ObjectWriter objectMapperWriter = StaticTestDependencies.getInstance().getObjectMapperWriter();

    @DisplayName("Given fields to enter for a custom immutable class, when creating a new object, and adding new element to a collection field after creation, collection field values did not change")
    @Test
    void testImmutableClassSample() throws JsonProcessingException {
        // Given
        String desc = "back at one";
        HashMap<String, String> stringMap = new HashMap<>();
        stringMap.put("one", "you're like a dream come true");
        stringMap.put("two", "just wanna be with you");

        // When
        ImmutableClassSample immutableClassSample = new ImmutableClassSample(desc, stringMap);
        logger.info("\nBefore immutable map update attempt: \n{}", objectMapperWriter.writeValueAsString(immutableClassSample));
        immutableClassSample.getImmutableMap().put("three","girl it's plain to see");
        logger.info("\nElement has been added to map field: \n{}", objectMapperWriter.writeValueAsString(immutableClassSample));

        // Then
        assertEquals("back at one", immutableClassSample.getDescription());
        assertEquals(desc, immutableClassSample.getDescription());
        logger.info("\nAfter immutable map update attempt: \n{}", objectMapperWriter.writeValueAsString(immutableClassSample));
    }

    @DisplayName("Given two immutable objects with same values, when comparing the two objects, both objects return the same hashCode")
    @Test
    void testImmutableClassWithSameValues(){
        // Given
        String desc = "back at one";
        HashMap<String, String> stringMap = new HashMap<>();
        stringMap.put("one", "you're like a dream come true");
        stringMap.put("two", "just wanna be with you");

        // When
        ImmutableClassSample immutableClassSample = new ImmutableClassSample(desc, stringMap);
        logger.info("\nImmutable class 1: \n{}", immutableClassSample.hashCode());
        logger.info("{}", immutableClassSample);
        ImmutableClassSample immutableClassSample1 = new ImmutableClassSample(desc, stringMap);
        logger.info("\nImmutable class 2: \n{}", immutableClassSample1.hashCode());
        logger.info("{}", immutableClassSample1);
        assertEquals(immutableClassSample, immutableClassSample1);
    }

    @DisplayName("Given two immutable wrapper classes with same values, when comparing the two objects, both objects return the same hashCode")
    @Test
    void testImmutableWrapperClassesWithSameValues(){
        Integer int1 = Integer.valueOf(11);
        logger.info("\nInteger object 1: \n{}", int1.hashCode());
        Integer int2 = Integer.valueOf(11);
        logger.info("\nInteger object 2: \n{}", int2.hashCode());

        String str1 = new String("same");
        logger.info("\nString object 1: \n{}",str1.hashCode());
        String str2 = new String("same");
        logger.info("\nString object 2: \n{}", str2.hashCode());

        Double dbl1 = Double.valueOf(12486);
        logger.info("\nDouble object 1: \n{}", dbl1.hashCode());
        Double dbl2 = Double.valueOf(12486);
        Double tmpDbl2before = dbl2;
        logger.info("\nDouble object 2: \n{}", dbl2.hashCode());
        dbl2 = Double.valueOf(12489);
        Double tmpDbl2after = dbl2;
        logger.info("\nDouble object 2: \n{}", dbl2.hashCode());
        logger.info("{} === {}", tmpDbl2before.hashCode(), tmpDbl2after.hashCode());
        assertNotEquals(tmpDbl2before, tmpDbl2after);
    }
}
